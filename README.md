# Tutorial 1: Deploy Smart Contract to Arbitrum 

In this Tutorial, we’ll teach you how to deploy a smart contract to Arbitrum, a leading Ethereum layer 2 scaling solution that uses optimistic rollups and enables high-throughput, low-cost smart contracts while remaining trustlessly secure.
## 1. Create a private Arbitrum Goerli RPC Endpoint 
Because the default RPC URL is a public RPC endpoint hosted by Arbitrum, anyone transacting on the Nitro testnet can use it. To avoid public endpoints, create a reliable and dedicated RPC endpoint just for your Arbitrum Nitro testnet transactions with Alchemy.  

### Step 1: Sign up for Alchemy  
Arbitrum developers can create a private RPC endpoint that is exclusive just for them instead of using the same public RPC endpoint.
This is an essential step. To make things easy, [sign up for a free Alchemy developer account](https://auth.alchemy.com/?redirectUrl=https%3A%2F%2Fdashboard.alchemyapi.io%2Fsignup%2F%3Fa%3D169b429af6).

### Step 2: Create an Arbitrum App  
Next, create a new app to generate an API key and connect to the Arbitrum Goerli test network.  
[https://docs.alchemy.com/docs/alchemy-quickstart-guide#1.create-an-alchemy-key](https://docs.alchemy.com/docs/alchemy-quickstart-guide#1.create-an-alchemy-key)  

![alt text](images/62e45526fb8319734a37d6c6_how-to-create-a-dapp-on-the-arbitrum-nitro-testnet.jpg)

## 2.	Connect to the Arbitrum Nitro Rollup Testnet
To start deploying smart contracts and interacting with test applications on the Arbitrum Nitro test network, the first step is to connect to the network in your wallet.  
Connect to the “Arbitrum Nitro Rollup Testnet” so you’re not dealing with real money.

### Step 3: Create an Ethereum wallet
We need an Ethereum wallet to send and receive transactions. For this tutorial, we’ll use MetaMask.

### Step 4: Copy your HTTP API Key

After creating your app, you should be able to see your Alchemy app’s chain matches the chain you want to connect to.  
Here, you can find the HTTP which you will use for your RPC URL in Metamask.    
![alt text](images/2bf06f0-The_chain.png)
### Step 5 : Create new network on Metamask
To add the "Arb-Goerli testnet" to MetaMask, click the network button at the top of your wallet and change it to __"Arbitrum Nitro Rollup Testnet"__.    
If the Arbitrum Nitro Goerli Rollup Testnet is not listed, click "Add Network," Fill in details about the new network you want to add.   
1. __Network Name__:  Arbitrum Nitro Rollup Testnet 
2. __RPC URL__: https://arb-goerli.g.alchemy.com/v2/YOUR-API-KEY
3. __ChainID__: 421613
4. __Symbol__: ETH
5. __Block Explorer URL__: https://goerli-rollup-explorer.arbitrum.io/
6. __Retryable Dashboard__: http://retryable-tx-panel-nitro.arbitrum.io/
7. __Token Bridge__: https://bridge.arbitrum.io/   

Replace the default RPC URL in MetaMask with your personal Arbitrum RPC endpoint.    
![alt text](images/metamask_reseau.png)
A simpler all in one solution would be to click the add network button. It will all the above steps.
![alt text](images/add_network_button.PNG)

## 3.	Bridge Goerli ETH to the Arbitrum Nitro Testnet
To execute transactions on the Goerli testnet you’ll need testnet ETH.    
### Step 6:  Get Goreli ETH
Goerli faucets: The Goerli Ethereum faucet is where developers can acquire Goerli testnet ETH for free.  
The first step is to enter your Ethereum wallet address for the Goerli network to Alchemy's Goerli Faucet to get free Goerli ETH.  
![alt text](images/a197b3e-Goerli_faucet.png)  

Next, open your MetaMask wallet and change your network to Arbitrum's Nitro Rollup Testnet.  

__Bridge Goreli ETH__  
After changing your network, bridge your Goerli L1 ETH to Arbitrum’s Goerli Testnet on layer 2 by connecting your wallet to the Arbitrum Goerli bridge.
[https://bridge.arbitrum.io/](https://bridge.arbitrum.io/)   
![alt text](images/4f8cf13-ETH_Goerli.png)  

Now, you should see Arbitrum Goerli ETH in your wallet and will be able to use your "Arb-Goerli testnet" ETH to execute contracts on the Arbitrum Nitro Rollup Testnet.  

## 4.	Deploy smart contract on Arbitrum 
### Step 8:  Initialize your Arbitrum project 
To initialize your Arbitrum  project, create a new folder by navigating to your command line and typing the following commands:
```
mkdir hello_arbitrum
cd hello_arbitrum
npm init
```
### Step 9 : Create Hardhat project 
Hardhat is a development environment to compile, deploy, test, and debug Ethereum software that helps developers build smart contracts and dApps locally.  
* __Install Hardhat__ 
[https://hardhat.org/hardhat-runner/docs/getting-started#overview](https://hardhat.org/hardhat-runner/docs/getting-started#overview)  
Inside your project run  
```
npm install --save-dev hardhat 
```
* __Create a new hardhat project__ 
```
npx hardhat
```
* __Install the dotenv package in your project directory__
```
npm install dotenv --save
```
* __Create  .env file in the root directory__
Define environment variables  
  - __Private key__: your MetaMask private key (prefix with 0x)
  Follow these instructions to [export your private key from MetaMask](https://metamask.zendesk.com/hc/en-us/articles/360015289632-How-to-Export-an-Account-Private-Key).    
  - __Layer 2 RPC__: HTTP Alchemy API URL.  

![alt_txt](images/Capture_d_écran_2022-10-14_120239.png)  

* __Update hardhat.config.js__  
To connect these to your code, we’ll reference these variables in your hardhat.config.js.  
![alt_txt](images/Capture_d_écran_2022-10-14_120848.png) 

* __Write your smart contract__  

![alt_txt](images/Capture_d_écran_2022-10-14_121032.png)

* __Compile your Contract__  
Compile your contract to make sure everything is working so far by running this command from the command line.  
```
npx hardhat compile
```
* __Write your deploy script__
Now that your contract is written and your configuration file is good to go, it’s time to write the contract deploy script.  
![alt_txt](images/Capture_d_écran_2022-10-14_123336.png) 

* __Deploy your smart contract__  
```
npx hardhat run scripts/deploy.js --network arbitrum
```
![alt_txt](images/Capture_d_écran_2022-10-14_122500.png)  

* __Publish and verify smart contract__
Copy contract address then navigate to https://goerli-rollup-explorer.arbitrum.io/  
![alt_txt](images/Capture_d_écran_2022-10-14_122900.png)  

## REF  

https://www.youtube.com/watch?v=LCZ0RiT0M_w&ab_channel=SmartContractProgrammer  
https://docs.alchemy.com/docs/arbitrum-nfts-creating-and-deploying-erc-721  
https://www.alchemy.com/overviews/arbitrum-nitro-testnet  
https://docs.alchemy.com/docs/how-to-add-arbitrum-to-metamask  
https://www.quicknode.com/guides/ethereum-development/how-to-create-and-deploy-a-smart-contract-with-hardhat







